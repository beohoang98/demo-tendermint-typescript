#!/bin/sh -e
if [ ! -f "$TMHOME/config/config.toml" ]
then
  (cp "$HOME/abci/config.toml" "$TMHOME/config/" && echo "Copied config file") || exit 1
fi
tendermint init validator || (echo "Error" && exit 2)
tendermint show-validator

/usr/bin/supervisord -n

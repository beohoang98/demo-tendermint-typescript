#!/bin/sh -x

protoc_command="protoc --plugin=node_modules/ts-proto/protoc-gen-ts_proto --ts_proto_opt=outputServices=grpc-js,esModuleInterop=true,env=node,useDate=true --ts_proto_out=./src/models -Iproto"

tendermint_files="$(find ./proto/tendermint -name "*.proto" | tr '\n' ' ')"

google_files="./proto/google/protobuf/duration.proto ./proto/google/protobuf/timestamp.proto ./proto/gogoproto/gogo.proto"

$protoc_command $tendermint_files $google_files

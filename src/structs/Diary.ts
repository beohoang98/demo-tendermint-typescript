import { Type } from "class-transformer";
import {
    IsDate,
    IsNotEmpty,
    IsOptional,
    IsString,
    IsUUID,
} from "class-validator";
import { Event } from "../models/tendermint/abci/types";

export class Diary {
    @IsUUID()
    @IsNotEmpty()
    id: string;

    @IsUUID()
    @IsNotEmpty()
    public productID: string;

    @IsUUID()
    @IsNotEmpty()
    public stepID: string;

    @IsUUID()
    @IsNotEmpty()
    public sectionID: string;

    @IsString()
    @IsNotEmpty()
    public name: string;

    @IsString()
    @IsOptional()
    public description: string;

    @IsDate()
    @Type(() => Date)
    createdAt: Date;

    public toEvent(): Event {
        return Event.fromPartial({
            type: "diary",
            attributes: [
                {
                    key: "id",
                    value: this.id,
                    index: true,
                },
                {
                    key: "product_id",
                    value: this.productID,
                    index: true,
                },
                {
                    key: "section_id",
                    value: this.sectionID,
                    index: true,
                },
                {
                    key: "step_id",
                    value: this.stepID,
                    index: true,
                },
                {
                    key: "created_at_timestamp",
                    value: this.createdAt.toISOString(),
                    index: true,
                },
            ],
        });
    }
}

import { IsNumber, IsString } from "class-validator";

export class Author {
    @IsNumber()
    public id: number;

    @IsString()
    public name: string;

    @IsString()
    public role: string;
}

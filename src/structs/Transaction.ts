import { IsDefined, IsEnum, ValidateNested } from "class-validator";
import { Event } from "../models/tendermint/abci/types";
import { Author } from "./Author";
import { Type } from "class-transformer";
import { Diary } from "./Diary";

export enum TransactionType {
    // eslint-disable-next-line no-unused-vars
    DIARY = "DIARY",
}

export enum TransactionAction {
    // eslint-disable-next-line no-unused-vars
    CREATE = "CREATE",
    // eslint-disable-next-line no-unused-vars
    UPDATE = "UPDATE",
    // eslint-disable-next-line no-unused-vars
    DELETE = "DELETE",
}

export class Transaction {
    @IsEnum(TransactionType)
    @IsDefined()
    type: TransactionType;

    @IsEnum(TransactionAction)
    @IsDefined()
    action: TransactionAction;

    @IsDefined()
    @Type(() => Diary)
    @ValidateNested()
    data: Diary;

    @IsDefined()
    @Type(() => Author)
    // @ValidateNested() // TODO: enable validate here when authentication is ready
    author: Author;

    public getEvent(): Event {
        return Event.fromPartial({
            type: "transaction",
            attributes: [
                { key: "type", value: this.type, index: true },
                { key: "action", value: this.action, index: true },
                { key: "author", value: this.author.id + "", index: true },
                { key: "id", value: this.data.id, index: true },
                {
                    key: "created_at",
                    value: this.data.createdAt.toISOString(),
                    index: true,
                },
            ],
        });
    }
}

import "reflect-metadata";

import { Server, ServerCredentials } from "@grpc/grpc-js";
import { ABCIApplicationService } from "./models/tendermint/abci/types";
import { ABCIApp } from "./ABCIApp";

const PORT = process.env.PORT || 26658;
const HOST = process.env.HOST || "0.0.0.0";

export const grpcServer = new Server();

grpcServer.addService(ABCIApplicationService, new ABCIApp());

grpcServer.bindAsync(
    `${HOST}:${PORT}`,
    ServerCredentials.createInsecure(),
    (err, port) => {
        console.log("Server start at port " + port);
        grpcServer.start();
    }
);

function exit() {
    console.log("Server shutdown");
    grpcServer.tryShutdown((error) => {
        if (error) {
            console.error(error);
            grpcServer.forceShutdown();
        }
    });
}

// Ctrl + C signal
process.on("SIGTERM", exit);
process.on("SIGINT", exit);
process.on("SIGBREAK", exit);
process.on("SIGHUP", exit);

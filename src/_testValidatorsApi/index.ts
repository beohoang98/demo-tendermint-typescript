import Express from "express";

const app = Express();

app.use(Express.json());
app.use(Express.urlencoded());

app.get("/validators", (req, res) => {
    return res.json({
        validators: [
            {
                address: "23192EA9221389226C26A0E84CF8CB423F506D4D",
                pub_key: {
                    type: "tendermint/PubKeyEd25519",
                    value: "aH52CdB5G34jpK1G4VU5dyJ7FmqAY8N9phKfglf7wXc=",
                },
                power: "69",
            },
            {
                address: "9484A909094C29CE18748747180D34BA6DFFA8BB",
                pub_key: {
                    type: "tendermint/PubKeyEd25519",
                    value: "1PT8qV+BMAqgtrVngvJ8HuC2wekleeu/mkEnes9UP7g=",
                },
                power: "69",
            },
            {
                address: "D436EC9FA862E09F6F10945C9D7178F3FE4930A0",
                pub_key: {
                    type: "tendermint/PubKeyEd25519",
                    value: "IRKZQDtXazl22Pb7Wwvj8S2U4bQIZ0tjytkhUF/ADXI=",
                },
                power: "69",
            },
        ],
    });
});

app.listen(+(process.env.PORT || 8080), "0.0.0.0", () => {
    console.log("Test server start at ", process.env.PORT || 8080);
});

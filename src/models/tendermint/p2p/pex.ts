/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "tendermint.p2p";

export interface PexAddress {
  id: string;
  ip: string;
  port: number;
}

export interface PexRequest {}

export interface PexResponse {
  addresses: PexAddress[];
}

export interface PexAddressV2 {
  url: string;
}

export interface PexRequestV2 {}

export interface PexResponseV2 {
  addresses: PexAddressV2[];
}

export interface PexMessage {
  pexRequest: PexRequest | undefined;
  pexResponse: PexResponse | undefined;
  pexRequestV2: PexRequestV2 | undefined;
  pexResponseV2: PexResponseV2 | undefined;
}

const basePexAddress: object = { id: "", ip: "", port: 0 };

export const PexAddress = {
  encode(
    message: PexAddress,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.ip !== "") {
      writer.uint32(18).string(message.ip);
    }
    if (message.port !== 0) {
      writer.uint32(24).uint32(message.port);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexAddress {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexAddress } as PexAddress;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.ip = reader.string();
          break;
        case 3:
          message.port = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PexAddress {
    const message = { ...basePexAddress } as PexAddress;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = "";
    }
    if (object.ip !== undefined && object.ip !== null) {
      message.ip = String(object.ip);
    } else {
      message.ip = "";
    }
    if (object.port !== undefined && object.port !== null) {
      message.port = Number(object.port);
    } else {
      message.port = 0;
    }
    return message;
  },

  toJSON(message: PexAddress): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.ip !== undefined && (obj.ip = message.ip);
    message.port !== undefined && (obj.port = message.port);
    return obj;
  },

  fromPartial(object: DeepPartial<PexAddress>): PexAddress {
    const message = { ...basePexAddress } as PexAddress;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = "";
    }
    if (object.ip !== undefined && object.ip !== null) {
      message.ip = object.ip;
    } else {
      message.ip = "";
    }
    if (object.port !== undefined && object.port !== null) {
      message.port = object.port;
    } else {
      message.port = 0;
    }
    return message;
  },
};

const basePexRequest: object = {};

export const PexRequest = {
  encode(_: PexRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexRequest } as PexRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): PexRequest {
    const message = { ...basePexRequest } as PexRequest;
    return message;
  },

  toJSON(_: PexRequest): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<PexRequest>): PexRequest {
    const message = { ...basePexRequest } as PexRequest;
    return message;
  },
};

const basePexResponse: object = {};

export const PexResponse = {
  encode(
    message: PexResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.addresses) {
      PexAddress.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexResponse } as PexResponse;
    message.addresses = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.addresses.push(PexAddress.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PexResponse {
    const message = { ...basePexResponse } as PexResponse;
    message.addresses = [];
    if (object.addresses !== undefined && object.addresses !== null) {
      for (const e of object.addresses) {
        message.addresses.push(PexAddress.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: PexResponse): unknown {
    const obj: any = {};
    if (message.addresses) {
      obj.addresses = message.addresses.map((e) =>
        e ? PexAddress.toJSON(e) : undefined
      );
    } else {
      obj.addresses = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<PexResponse>): PexResponse {
    const message = { ...basePexResponse } as PexResponse;
    message.addresses = [];
    if (object.addresses !== undefined && object.addresses !== null) {
      for (const e of object.addresses) {
        message.addresses.push(PexAddress.fromPartial(e));
      }
    }
    return message;
  },
};

const basePexAddressV2: object = { url: "" };

export const PexAddressV2 = {
  encode(
    message: PexAddressV2,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.url !== "") {
      writer.uint32(10).string(message.url);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexAddressV2 {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexAddressV2 } as PexAddressV2;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.url = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PexAddressV2 {
    const message = { ...basePexAddressV2 } as PexAddressV2;
    if (object.url !== undefined && object.url !== null) {
      message.url = String(object.url);
    } else {
      message.url = "";
    }
    return message;
  },

  toJSON(message: PexAddressV2): unknown {
    const obj: any = {};
    message.url !== undefined && (obj.url = message.url);
    return obj;
  },

  fromPartial(object: DeepPartial<PexAddressV2>): PexAddressV2 {
    const message = { ...basePexAddressV2 } as PexAddressV2;
    if (object.url !== undefined && object.url !== null) {
      message.url = object.url;
    } else {
      message.url = "";
    }
    return message;
  },
};

const basePexRequestV2: object = {};

export const PexRequestV2 = {
  encode(
    _: PexRequestV2,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexRequestV2 {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexRequestV2 } as PexRequestV2;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): PexRequestV2 {
    const message = { ...basePexRequestV2 } as PexRequestV2;
    return message;
  },

  toJSON(_: PexRequestV2): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<PexRequestV2>): PexRequestV2 {
    const message = { ...basePexRequestV2 } as PexRequestV2;
    return message;
  },
};

const basePexResponseV2: object = {};

export const PexResponseV2 = {
  encode(
    message: PexResponseV2,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.addresses) {
      PexAddressV2.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexResponseV2 {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexResponseV2 } as PexResponseV2;
    message.addresses = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.addresses.push(PexAddressV2.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PexResponseV2 {
    const message = { ...basePexResponseV2 } as PexResponseV2;
    message.addresses = [];
    if (object.addresses !== undefined && object.addresses !== null) {
      for (const e of object.addresses) {
        message.addresses.push(PexAddressV2.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: PexResponseV2): unknown {
    const obj: any = {};
    if (message.addresses) {
      obj.addresses = message.addresses.map((e) =>
        e ? PexAddressV2.toJSON(e) : undefined
      );
    } else {
      obj.addresses = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<PexResponseV2>): PexResponseV2 {
    const message = { ...basePexResponseV2 } as PexResponseV2;
    message.addresses = [];
    if (object.addresses !== undefined && object.addresses !== null) {
      for (const e of object.addresses) {
        message.addresses.push(PexAddressV2.fromPartial(e));
      }
    }
    return message;
  },
};

const basePexMessage: object = {};

export const PexMessage = {
  encode(
    message: PexMessage,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.pexRequest !== undefined) {
      PexRequest.encode(message.pexRequest, writer.uint32(10).fork()).ldelim();
    }
    if (message.pexResponse !== undefined) {
      PexResponse.encode(
        message.pexResponse,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.pexRequestV2 !== undefined) {
      PexRequestV2.encode(
        message.pexRequestV2,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.pexResponseV2 !== undefined) {
      PexResponseV2.encode(
        message.pexResponseV2,
        writer.uint32(34).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PexMessage {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePexMessage } as PexMessage;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pexRequest = PexRequest.decode(reader, reader.uint32());
          break;
        case 2:
          message.pexResponse = PexResponse.decode(reader, reader.uint32());
          break;
        case 3:
          message.pexRequestV2 = PexRequestV2.decode(reader, reader.uint32());
          break;
        case 4:
          message.pexResponseV2 = PexResponseV2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PexMessage {
    const message = { ...basePexMessage } as PexMessage;
    if (object.pexRequest !== undefined && object.pexRequest !== null) {
      message.pexRequest = PexRequest.fromJSON(object.pexRequest);
    } else {
      message.pexRequest = undefined;
    }
    if (object.pexResponse !== undefined && object.pexResponse !== null) {
      message.pexResponse = PexResponse.fromJSON(object.pexResponse);
    } else {
      message.pexResponse = undefined;
    }
    if (object.pexRequestV2 !== undefined && object.pexRequestV2 !== null) {
      message.pexRequestV2 = PexRequestV2.fromJSON(object.pexRequestV2);
    } else {
      message.pexRequestV2 = undefined;
    }
    if (object.pexResponseV2 !== undefined && object.pexResponseV2 !== null) {
      message.pexResponseV2 = PexResponseV2.fromJSON(object.pexResponseV2);
    } else {
      message.pexResponseV2 = undefined;
    }
    return message;
  },

  toJSON(message: PexMessage): unknown {
    const obj: any = {};
    message.pexRequest !== undefined &&
      (obj.pexRequest = message.pexRequest
        ? PexRequest.toJSON(message.pexRequest)
        : undefined);
    message.pexResponse !== undefined &&
      (obj.pexResponse = message.pexResponse
        ? PexResponse.toJSON(message.pexResponse)
        : undefined);
    message.pexRequestV2 !== undefined &&
      (obj.pexRequestV2 = message.pexRequestV2
        ? PexRequestV2.toJSON(message.pexRequestV2)
        : undefined);
    message.pexResponseV2 !== undefined &&
      (obj.pexResponseV2 = message.pexResponseV2
        ? PexResponseV2.toJSON(message.pexResponseV2)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<PexMessage>): PexMessage {
    const message = { ...basePexMessage } as PexMessage;
    if (object.pexRequest !== undefined && object.pexRequest !== null) {
      message.pexRequest = PexRequest.fromPartial(object.pexRequest);
    } else {
      message.pexRequest = undefined;
    }
    if (object.pexResponse !== undefined && object.pexResponse !== null) {
      message.pexResponse = PexResponse.fromPartial(object.pexResponse);
    } else {
      message.pexResponse = undefined;
    }
    if (object.pexRequestV2 !== undefined && object.pexRequestV2 !== null) {
      message.pexRequestV2 = PexRequestV2.fromPartial(object.pexRequestV2);
    } else {
      message.pexRequestV2 = undefined;
    }
    if (object.pexResponseV2 !== undefined && object.pexResponseV2 !== null) {
      message.pexResponseV2 = PexResponseV2.fromPartial(object.pexResponseV2);
    } else {
      message.pexResponseV2 = undefined;
    }
    return message;
  },
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

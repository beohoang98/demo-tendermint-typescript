/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  PubKeyRequest,
  PubKeyResponse,
  SignVoteRequest,
  SignedVoteResponse,
  SignProposalRequest,
  SignedProposalResponse,
} from "../../tendermint/privval/types";

export const protobufPackage = "tendermint.privval";

export const PrivValidatorAPIService = {
  getPubKey: {
    path: "/tendermint.privval.PrivValidatorAPI/GetPubKey",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: PubKeyRequest) =>
      Buffer.from(PubKeyRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => PubKeyRequest.decode(value),
    responseSerialize: (value: PubKeyResponse) =>
      Buffer.from(PubKeyResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => PubKeyResponse.decode(value),
  },
  signVote: {
    path: "/tendermint.privval.PrivValidatorAPI/SignVote",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SignVoteRequest) =>
      Buffer.from(SignVoteRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => SignVoteRequest.decode(value),
    responseSerialize: (value: SignedVoteResponse) =>
      Buffer.from(SignedVoteResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => SignedVoteResponse.decode(value),
  },
  signProposal: {
    path: "/tendermint.privval.PrivValidatorAPI/SignProposal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SignProposalRequest) =>
      Buffer.from(SignProposalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => SignProposalRequest.decode(value),
    responseSerialize: (value: SignedProposalResponse) =>
      Buffer.from(SignedProposalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SignedProposalResponse.decode(value),
  },
} as const;

export interface PrivValidatorAPIServer extends UntypedServiceImplementation {
  getPubKey: handleUnaryCall<PubKeyRequest, PubKeyResponse>;
  signVote: handleUnaryCall<SignVoteRequest, SignedVoteResponse>;
  signProposal: handleUnaryCall<SignProposalRequest, SignedProposalResponse>;
}

export interface PrivValidatorAPIClient extends Client {
  getPubKey(
    request: PubKeyRequest,
    callback: (error: ServiceError | null, response: PubKeyResponse) => void
  ): ClientUnaryCall;
  getPubKey(
    request: PubKeyRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: PubKeyResponse) => void
  ): ClientUnaryCall;
  getPubKey(
    request: PubKeyRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: PubKeyResponse) => void
  ): ClientUnaryCall;
  signVote(
    request: SignVoteRequest,
    callback: (error: ServiceError | null, response: SignedVoteResponse) => void
  ): ClientUnaryCall;
  signVote(
    request: SignVoteRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: SignedVoteResponse) => void
  ): ClientUnaryCall;
  signVote(
    request: SignVoteRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: SignedVoteResponse) => void
  ): ClientUnaryCall;
  signProposal(
    request: SignProposalRequest,
    callback: (
      error: ServiceError | null,
      response: SignedProposalResponse
    ) => void
  ): ClientUnaryCall;
  signProposal(
    request: SignProposalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SignedProposalResponse
    ) => void
  ): ClientUnaryCall;
  signProposal(
    request: SignProposalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SignedProposalResponse
    ) => void
  ): ClientUnaryCall;
}

export const PrivValidatorAPIClient = makeGenericClientConstructor(
  PrivValidatorAPIService,
  "tendermint.privval.PrivValidatorAPI"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): PrivValidatorAPIClient;
};

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

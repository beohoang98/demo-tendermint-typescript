import {
    ABCIApplicationServer,
    RequestApplySnapshotChunk,
    RequestBeginBlock,
    RequestCheckTx,
    RequestCommit,
    RequestDeliverTx,
    RequestEcho,
    RequestEndBlock,
    RequestFlush,
    RequestInfo,
    RequestInitChain,
    RequestListSnapshots,
    RequestLoadSnapshotChunk,
    RequestOfferSnapshot,
    RequestQuery,
    ResponseApplySnapshotChunk,
    ResponseBeginBlock,
    ResponseCheckTx,
    ResponseCommit,
    ResponseDeliverTx,
    ResponseEcho,
    ResponseEndBlock,
    ResponseFlush,
    ResponseInfo,
    ResponseInitChain,
    ResponseListSnapshots,
    ResponseLoadSnapshotChunk,
    ResponseOfferSnapshot,
    ResponseQuery,
} from "./models/tendermint/abci/types";
import {
    sendUnaryData,
    ServerUnaryCall,
    UntypedHandleCall,
} from "@grpc/grpc-js";
import crypto from "crypto";
import { Buffer } from "buffer";
import axios from "axios";
import { ClassTransformer, plainToClass } from "class-transformer";
import { validateOrReject, ValidationError, Validator } from "class-validator";
import { Transaction } from "./structs/Transaction";

const CONFIG = {
    USING_VALIDATOR_API: !!process.env.USING_VALIDATOR_API || false,
    VALIDATOR_ENDPOINT: process.env.VALIDATOR_ENDPOINT,
    FIXED_POWER: 69,
};

export class ABCIApp implements ABCIApplicationServer {
    // eslint-disable-next-line no-undef
    [name: string]: UntypedHandleCall;

    initChain(
        call: ServerUnaryCall<RequestInitChain, ResponseInitChain>,
        callback: sendUnaryData<ResponseInitChain>
    ): void {
        console.debug(call.request);
        function returnCallback(validators?: any[]) {
            callback(
                null,
                ResponseInitChain.fromPartial({
                    appHash: crypto
                        .createHash("sha256")
                        .update("co 2 cai cay trong mot cai vuon")
                        .digest(),
                    validators,
                    consensusParams: call.request.consensusParams,
                })
            );
        }
        if (CONFIG.USING_VALIDATOR_API) {
            if (!CONFIG.VALIDATOR_ENDPOINT) {
                return callback(new Error("VALIDATOR_ENDPOINT not set yet"));
            }

            axios
                .create()
                .get(CONFIG.VALIDATOR_ENDPOINT)
                .then(({ data }) => {
                    const { validators } = data;
                    console.debug(data);
                    return returnCallback(
                        validators.map((val) => ({
                            pubKey: {
                                ed25519: Buffer.from(
                                    val.pub_key.value,
                                    "base64"
                                ),
                            },
                            power: CONFIG.FIXED_POWER,
                        }))
                    );
                })
                .catch((error) => {
                    console.error(error);
                    callback(error);
                });
        }
        return returnCallback();
    }

    applySnapshotChunk(
        call: ServerUnaryCall<
            RequestApplySnapshotChunk,
            ResponseApplySnapshotChunk
        >,
        callback: sendUnaryData<ResponseApplySnapshotChunk>
    ): void {
        callback(null, ResponseApplySnapshotChunk.fromPartial({}));
    }

    beginBlock(
        call: ServerUnaryCall<RequestBeginBlock, ResponseBeginBlock>,
        callback: sendUnaryData<ResponseBeginBlock>
    ): void {
        callback(null, ResponseBeginBlock.fromPartial({}));
    }

    async checkTx(
        call: ServerUnaryCall<RequestCheckTx, ResponseCheckTx>,
        callback: sendUnaryData<ResponseCheckTx>
    ) {
        try {
            const validator = new Validator();
            const transformer = new ClassTransformer();
            const txData = call.request.tx.toString("utf8");
            console.log(txData);
            const tx = transformer.plainToClass(
                Transaction,
                JSON.parse(txData)
            );
            console.log("Begin check tx", tx);
            await validator.validateOrReject(tx);

            callback(
                null,
                ResponseCheckTx.fromPartial({
                    code: 0,
                    data: call.request.tx,
                })
            );
        } catch (e) {
            console.error(e);
            if (e instanceof ValidationError) {
                return callback(
                    null,
                    ResponseCheckTx.fromPartial({
                        code: 1,
                        log: e.toString(true),
                    })
                );
            }
            callback(
                null,
                ResponseCheckTx.fromPartial({
                    code: 2,
                    log: e + "",
                })
            );
        }
    }

    commit(
        call: ServerUnaryCall<RequestCommit, ResponseCommit>,
        callback: sendUnaryData<ResponseCommit>
    ): void {
        callback(null, ResponseCommit.fromPartial({}));
    }

    async deliverTx(
        call: ServerUnaryCall<RequestDeliverTx, ResponseDeliverTx>,
        callback: sendUnaryData<ResponseDeliverTx>
    ) {
        try {
            const txData = call.request.tx.toString("utf8");
            const tx = plainToClass(Transaction, JSON.parse(txData) as Object);
            await validateOrReject(tx);
            callback(
                null,
                ResponseDeliverTx.fromPartial({
                    code: 0,
                    data: call.request.tx,
                    events: [tx.getEvent(), tx.data.toEvent()],
                })
            );
        } catch (e) {
            console.error(e);
            if (e instanceof ValidationError) {
                return callback(
                    null,
                    ResponseCheckTx.fromPartial({
                        code: 1,
                        log: e.toString(true),
                    })
                );
            }
            callback(
                null,
                ResponseCheckTx.fromPartial({
                    code: 2,
                    log: e + "",
                })
            );
        }
    }

    echo(
        call: ServerUnaryCall<RequestEcho, ResponseEcho>,
        callback: sendUnaryData<ResponseEcho>
    ): void {
        callback(
            null,
            ResponseEcho.fromPartial({
                message: call.request.message,
            })
        );
    }

    endBlock(
        call: ServerUnaryCall<RequestEndBlock, ResponseEndBlock>,
        callback: sendUnaryData<ResponseEndBlock>
    ): void {
        callback(null, ResponseEndBlock.fromPartial({}));
    }

    flush(
        call: ServerUnaryCall<RequestFlush, ResponseFlush>,
        callback: sendUnaryData<ResponseFlush>
    ): void {
        callback(null, ResponseFlush.fromPartial({}));
    }

    info(
        call: ServerUnaryCall<RequestInfo, ResponseInfo>,
        callback: sendUnaryData<ResponseInfo>
    ): void {
        console.debug(call.request);
        callback(null, ResponseInfo.fromPartial({}));
    }

    listSnapshots(
        call: ServerUnaryCall<RequestListSnapshots, ResponseListSnapshots>,
        callback: sendUnaryData<ResponseListSnapshots>
    ): void {
        callback(
            null,
            ResponseListSnapshots.fromPartial({
                snapshots: [],
            })
        );
    }

    loadSnapshotChunk(
        call: ServerUnaryCall<
            RequestLoadSnapshotChunk,
            ResponseLoadSnapshotChunk
        >,
        callback: sendUnaryData<ResponseLoadSnapshotChunk>
    ): void {
        callback(null, ResponseLoadSnapshotChunk.fromPartial({}));
    }

    offerSnapshot(
        call: ServerUnaryCall<RequestOfferSnapshot, ResponseOfferSnapshot>,
        callback: sendUnaryData<ResponseOfferSnapshot>
    ): void {
        callback(null, ResponseOfferSnapshot.fromPartial({}));
    }

    query(
        call: ServerUnaryCall<RequestQuery, ResponseQuery>,
        callback: sendUnaryData<ResponseQuery>
    ): void {
        callback(
            null,
            ResponseQuery.fromPartial({
                code: 0,
                log: "test",
            })
        );
    }
}

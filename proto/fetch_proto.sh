#!/bin/sh

svn checkout --force --no-auth-cache https://github.com/tendermint/tendermint/trunk/proto/tendermint/

svn checkout --force --no-auth-cache https://github.com/gogo/protobuf/trunk/gogoproto/

svn checkout --force --no-auth-cache https://github.com/protocolbuffers/protobuf/trunk/src/google/protobuf google/protobuf

find . -type f ! -name "*.proto" ! -name "fetch_proto.sh" -delete

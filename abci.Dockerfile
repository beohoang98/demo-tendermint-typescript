FROM node:14-alpine as builder
WORKDIR /build
COPY package.json .
RUN yarn
COPY . .
RUN yarn build

FROM node:14-alpine as runner
RUN adduser -D abci
USER abci
WORKDIR /home/abci
COPY --from=builder --chown=abci /build/dist /home/abci/dist
COPY . .
RUN yarn install --production
CMD node dist/index.js

EXPOSE ${PORT:-26658}

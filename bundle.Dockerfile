FROM tendermint/tendermint:latest
USER root
RUN apk add --update nodejs py-pip npm
RUN pip install supervisor

RUN adduser -D tendermint

USER tendermint
ENV HOME=/home/tendermint
ENV TMHOME=$HOME/tendermint

## Genesis.json will be automatically generated
## So, when deploy, we need mount the genesis.json into this container

RUN ["/bin/bash", "-c", "mkdir -p $TMHOME/{config,data} $HOME/{abci,log}"]
COPY --chown=tendermint config.toml $TMHOME/config/
RUN ls -alR $TMHOME

WORKDIR $HOME/abci/
COPY package.json .
RUN npm i
COPY --chown=tendermint . .
RUN npm run build

COPY supervisord.conf /etc/supervisor/conf.d/

ENTRYPOINT ["./entrypoint.sh"]

#abci
EXPOSE ${PORT:-26658}

# p2p
EXPOSE 26656

# rpc
EXPOSE 26657

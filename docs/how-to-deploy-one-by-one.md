# HOW TO DEPLOY (ONE-BY-ONE)

## 1. ABCI and Tendermint Core

Of each node, usually of each VPS, we will have 2 applications:

- **ABCI App**: for transaction logic:
    
    ABCI is a NodeJs app, so it will be built on Node container

- **Tendermint Core**: for blockchain logic and peer-to-peer:
    
    We will pull it from `tendermint/tendermint:latest` image.
  
## 2. Config and Data

- ABCI app don't store any data.

- Tendermint will store config at `$TMHOME/config` and data at `$TMHOME/data`.

    - Config: we have important configs are `node_key.json`, `config.toml`,
    `genesis.json` and `priv_validator_key.json`. It **must** be mounted from inside container
      to local VPS filesystem.
      
    ```yaml
    # example with docker-compose
    services:
      tendermint:
        # ...
        volumes:
          - "$CONFIG_DIR:/tendermint/config"
          - "$DATA_DIR:/tendermint/data"
    ```
    ```shell
    # docker run example
    docker run -d \
        --volume "$CONFIG_DIR:/tendermint/config" \
        --volume "$DATA_DIR:/tendermint/data" \
        -p 26656:26656 -p 26657:26657 \
        tendermint/tendermint:latest
    ```

## 3. Peer 2 Peer config

When tendermint core is running, it will generate:

- `node_key.json`: Store the validator's `ID`, we will get this `ID` to
share with other node's config

- `priv_validator_key.json`: Store the validator's `address` and `pubKey`

- `config.toml`: Config of the node, 
  but there are some suitable config we **must** change from default

    ```toml
    proxy-app = "{abci_host}:26658"
    mode = "validator"
  
    [rpc]
    addr = "tcp://0.0.0.0:26657"
  
    [p2p]
    persistent_peers = "{OTHER_ID}@{OTHER_HOST}:26656, ..."
  
    [consensus]
    create-empty-block = false
    ```

## 4. Validators set

There are 2 ways:

1. Store validators set in server, then abci will query it.

  - Set environment variable `USING_VALIDATOR_ENDPOINT=true` 
    and `VALIDATOR_ENDPOINT=...` to tell ABCI app using this way.

2. Store it in `genesis.json`, but `genesis.json` must be **same** on all nodes

  - Get all `address` and `pubKey` of each node in `priv_validator_key.json`

  - Put it into `genesis.json`

  - Copy `genesis.json` into all nodes.

# TRANSACTION DATA

> This document is written for define which data will be stored
> inside a transaction and also will be included into blockchain

## Data column

> File [Transaction.ts](../src/structs/Transaction.ts)

```yaml
transaction:
    action: "CREATE|UPDATE|DELETE"
    type: "DIARY" # still using, will be extended in future 
    data: raw_string
    author:
        id: string|number
        name: string
```

```yaml
diary:
    id: uuid
    
    productId: uuid
    stepPropertyID: uuid
    sectionID: uuid
    
    name: string
    description: string
    created_at: date string
```

## Data type

### Send

```json5
# POST /
{
    "jsonrpc": "2.0",
    "id": "something",
    "method": "broadcast_tx_commit",
    "params": {
        "tx": "<base_64_json_string>",
    }
}
```

### Receive
```json5
{
    "jsonrpc": "2.0",
    "id": "<id from request>",
    "result": {
        "check_tx": {
            "code": 0 | 1 | 2, // 0 is success
            "log": "<string>"
        },
        "check_tx": {
            "code": 0 | 1 | 2, // 0 is success
            "log": "<string>"
        },
        "hash": "<hex_string>",
        "height": "123"
    }
}
```

Using `hash` and `height` to save back to `Diary` in database

## Query data

You can also query back data we pushed

```json5
# POST /
{
    "jsonrpc": "2.0",
    "id": "something",
    "method": "tx_search",
    "params": {
        "query": "<query>",
    }
}
```

`query` can be `tx.hash='ABC1234560ABC...'` or `transaction.action='CREATE'`
or `diary.product_id='1234-4567-abcd-...'`

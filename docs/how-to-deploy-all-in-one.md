# HOW TO DEPLOY GUIDE (ALL-IN-ONE CONTAINER)
> With docker

## 0.1. Pull image and start central node

> "Central Node" will be a consistent and trusty node that
> other nodes will be connect to.

```shell
docker pull farmhub/abci_node:${TAG}
```

> TAG might be `latest`, `develop` or a version number

**Then**, start by

```shell
docker run -d \
  --volume "$CONFIG_DIR:/home/tendermint/tendermint/config" \
  --volume "$DATA_DIR:/home/tendermint/tendermint/data" \
  --publish "26657:26657" \
  --publish "26656:26656" \
  farm_hub/abci_node:${TAG}
```

> `$CONFIG_DIR` and `$DATA_DIR` is the **absolute path** in local/vps machine
> 
> Store the data and config independence with docker container.
>
> 26657 and 26656 can be overridden

## 0.2. Build from Dockerfile

Another way that we can build from Dockerfile

```shell
docker build . -f bundle.Dockerfile -t farmhub/abci_node:build_local
```

**Then**, start by

```shell
docker run -d \
  --volume "$CONFIG_DIR:/home/tendermint/tendermint/config" \
  --volume "$DATA_DIR:/home/tendermint/tendermint/data" \
  --publish "26657:26657" \
  --publish "26656:26656" \
  farm_hub/abci_node:build_local
```

> `$CONFIG_DIR` and `$DATA_DIR` is the **absolute path** in local/vps machine
>
> Store the data and config independence with docker container.
>
> 26657 and 26656 can be overridden

## 1. Add new node (new validators)

Network diagram will be like this:

![Network Diagram](blockchain-service.svg)

1. New Validator will be start in new Machine/VPS

2. The `address` and `publicKey` of Validator 
   can be got at `{vps_host}:26657/status`
   or in `$CONFIG_DIR/priv_validator_key.json` inside docker.
   
   ```json5
   /** example */
   {
      "address": "23192EA9221389226C26A0E84CF8CB423F506D4D",
      "pub_key": {
         "type": "tendermint/PubKeyEd25519",
         "value": "aH52CdB5G34jpK1G4VU5dyJ7FmqAY8N9phKfglf7wXc="
      }
   }
   ```
   
   **Then**, the new node must connect to central node:

   ```toml
   # $CONFIG_DIR/config.toml
   # ...
   [p2p]
   # ...
   persistent_peers = "{NODE_ID}@{NODE_HOST}:{port}"
   # example: "abcef0959absdaw@192.168.69.69:26656"
   # ...
   ```

   > NODE_HOST is IP of the central node
   > 
   > NODE_ID is ID of central tendermint node,
   > can be get at `{host}:26657/status` or `$CONFIG_DIR/node_key.json`.


3. New `address` and `pub_key` will be added in to
   a central server (also can be Farm Service) 
   that blockchain nodes will ask to get validators.
   
   **Example:**
   ```
   Node: Starting...
   Node: Ask validators
   Central Server: Response validators list
   Node: Set validators into peers set and trust them
   Node: work normal...
   ```

4. Order deploying:

```
1. Central Server
2. Central node
3. Other nodes..., with config has `consistent_peers=${central_node_url}`
```

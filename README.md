> # IN PROGRESS
> In updating regular

# Blockchain Service

|Role|Using|
|:---|:---|
|**Blockchain**|[Tendermint Core](https://docs.tendermint.com)|
|**ABCI Layer**|Nodejs with Typescript and Grpc server|

## Demo

### 0. Install dependencies

- [Docker](https://docker.com)

- [NodeJS](https://nodejs.org) (14+)

- [Protoc](https://github.com/protocolbuffers/protobuf/releases) (Protobuf compiler)

-
    ```sh
    yarn install
    # or
    npm install
    ```

### 1. Download proto files

```sh
cd proto
./fetch_proto.sh
```

### 2. Generate models from proto files

```sh
# at root folder
./extract_proto.sh
```

Models will be generated to `src/models`

### 3. Building ABCI app in docker-compose

```sh
docker-compose build
```

### 4. Start nodes and abci

```sh
docker-compose up -d
```

### 5. Test

```shell
curl -v 'http://127.0.0.1:26657/status'
# ...
curl -v 'http://127.0.0.1:26657/broadcast_tx_commit?tx="some txt data"'
```

## Building All-in-one Docker image

```shell
docker build -f bundle.Dockerfile -t farmhub/abci_node:${TAG} .
```

**Note**: TAG can be `develop`, `develop-deploy`, `latest`, `stable-deploy`, or a version number, ...

## Deploy nodes

- [One by one in VPS](docs/how-to-deploy-one-by-one.md)
  
- [All in one docker container](docs/how-to-deploy-all-in-one.md)

## Other documents

- [Transaction definition](docs/transaction-data.md)
